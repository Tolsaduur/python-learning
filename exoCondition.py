year = input('Saissisez une année : ')
intYear = int(year)

# On test si l'année est divisible par 4 et non divisible par 100, ou si l'année est divisible par 400.
if (intYear % 4 == 0 and intYear % 100 != 0) or intYear % 400 == 0:
    print('Oui, '+year+' est une année bissextile.');
else:
    print('Non, '+year+' n\'est pas une année bissextile.');
